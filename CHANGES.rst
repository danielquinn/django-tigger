Changes
#######

1.1.0
=====

* Allow for different URL styles in the ``url=`` argument to ``Bounce()``.


1.0.0
=====

* Initial release.
